import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class TranscriberTests {
    @Test
    @DisplayName("Given expected input should return correct text in Morse Code")
    fun transcribeToMorse_givenText_shouldReturnMorseCode() {
        val input = "This is Commander Riker, send help"
        val result = Transcriber.toMorse(input)
        val expected = "-|....|..|.../..|.../-.-.|---|--|--|.-|-.|-..|.|.-./.-.|..|-.-|.|.-.|--..--/...|.|-.|-../....|.|.-..|.--."
        Assertions.assertEquals(expected, result)
    }

    @Test
    @DisplayName("Given expected input should return correct text in Morse Code")
    fun transcribeToMorse_givenText_shouldReturnMorseCode2() {
        val input = "I AM IN TROUBLE"
        val result = Transcriber.toMorse(input)
        val expected = "../.-|--/..|-./-|.-.|---|..-|-...|.-..|."
        Assertions.assertEquals(expected, result)
    }

    @Test
    @DisplayName("Given incorrect input should return null")
    fun transcribeToMorse_givenUnexpectedText_shouldNull() {
        val input = "These are unsupported åç]["
        val result = Transcriber.toMorse(input)
        val expected = null
        Assertions.assertEquals(expected, result)
    }
}