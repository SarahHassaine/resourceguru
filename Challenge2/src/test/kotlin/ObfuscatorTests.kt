import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class ObfuscatorTests {
    @Test
    @DisplayName("Given expected input should return obfuscated text")
    fun obfuscate_givenCorrectInput_returnsObfuscatedText() {
        val input = "../.-|--/..|-./-|.-.|---|..-|-...|.-..|."
        val result = Obfuscator.obfuscate(input)
        val expected = "2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1"
        Assertions.assertEquals(expected, result)
    }

    @Test
    @DisplayName("Given empty input should return empty text")
    fun obfuscate_givenEmptyInput_returnsEmptyText() {
        val input = ""
        val result = Obfuscator.obfuscate(input)
        val expected = ""
        Assertions.assertEquals(expected, result)
    }

    @Test
    @DisplayName("Given invalid input should obfuscate what it can")
    fun obfuscate_givenInvalidInput_returnsObfuscatedText() {
        val input = "../.-|--/..|-./-|.-.|---|..-|-...|.-..|.78Test"
        val result = Obfuscator.obfuscate(input)
        val expected = "2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|178Test"
        Assertions.assertEquals(expected, result)
    }
}