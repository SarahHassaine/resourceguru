
class Obfuscator {

    companion object {
        fun obfuscate(input: String): String {
            val replacedDots = Regex("[.]+").replace(input) {
                m -> m.value.length.toString()
            }

            val replecedtDashes = Regex("[-]+").replace(replacedDots) {
                m -> numberToCharacter(m.value.length).toString()
            }

            return replecedtDashes
        }

        private fun numberToCharacter(number: Int): Char {
            return (number + 64).toChar()
        }
    }
}