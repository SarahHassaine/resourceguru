
import java.io.File

class App {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            do {
                println("Please choose:")
                println("1. Enter file path")
                println("2. Enter text directly")
                println("3. End program")
                val choiceInput = readLine()

                when (choiceInput) {
                    "1" -> transcribeFile()
                    "2" -> transcribeInput()
                }

            } while (choiceInput != "3")

        }


        private fun transcribeFile() {
            println("Enter filename:")
            val fileName = readLine()
            val file = File(fileName)
            if (file.exists()) {
                val outputLines = getObfuscatedLines(file)
                printToFile(outputLines)
            } else {
                println("File doesn't exist")
            }
        }

        private fun getObfuscatedLines(file: File): ArrayList<String> {
            val outputLines = ArrayList<String>()
            file.readLines().forEach {
                val morse = Transcriber.toMorse(it)
                if (morse != null) {
                    val obfuscated = Obfuscator.obfuscate(morse)
                    outputLines.add(obfuscated)
                } else {
                    println("Invalid characters used on this line")
                }
            }
            return outputLines
        }

        private fun printToFile(outputLines: ArrayList<String>) {
            val outputFile = File("result.txt")
            outputFile.createNewFile()

            outputFile.bufferedWriter().use { out ->
                outputLines.forEach {
                    out.write(it)
                    out.newLine()
                }
            }
            println("Output in result.txt")
        }

        private fun transcribeInput() {
            println("Enter text")
            val text = readLine()
            val morse = text?.let { Transcriber.toMorse(it) }
            if (morse != null) {
                val obfuscated = Obfuscator.obfuscate(morse)
                println(obfuscated)
            } else {
                println("Invalid characters used in string")
            }
        }

    }
}
