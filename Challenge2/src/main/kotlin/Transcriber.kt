
class Transcriber {

    companion object {
        fun toMorse(input: String): String? {
            var morse = ""
            val words = input.split(" ")

            for (word in words) {
                for (item in word.indices) {
                    val char: String = word[item].toUpperCase().toString()
                    val morseChar = morseLookupTable[char] ?: return null
                    morse += morseChar
                    if (item < word.length - 1) {
                        morse += "|"
                    }
                }
                if (words.last() != word) {
                    morse += "/"
                }
            }
            return morse
        }


        private val morseLookupTable = hashMapOf(
            "A" to ".-",
            "B" to "-...",
            "C" to "-.-.",
            "D" to "-..",
            "E" to ".",
            "F" to "..-.",
            "G" to "--.",
            "H" to "....",
            "I" to "..",
            "J" to ".---",
            "K" to "-.-",
            "L" to ".-..",
            "M" to "--",
            "N" to "-.",
            "O" to "---",
            "P" to ".--.",
            "Q"	to "--.-",
            "R" to ".-.",
            "S" to "...",
            "T" to "-",
            "U" to "..-",
            "V" to "...-",
            "W" to ".--",
            "X" to "-..-",
            "Y" to "-.--",
            "Z" to "--..",
            "." to ".-.-.-",
            "," to "--..--")
    }
}