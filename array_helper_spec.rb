require './array_helper'

RSpec.describe ArrayHelper do
  describe "#flatten" do
    it "returns flat array given any arbitrarly nested array" do
      game = ArrayHelper.new
      expect(game.flatten([ 1, [ 2, [ 3 ] ], 4 ])).to eq([ 1, 2, 3, 4 ])
      expect(game.flatten([ 1, [ 2, [ 3, 7, 8, [9, 10], 11 ], [12, 13] ], 4 ])).to eq([ 1, 2, 3, 7, 8, 9, 10, 11, 12, 13, 4 ])
      expect(game.flatten([ 1, 2, 3 ])).to eq([ 1, 2, 3 ])
      expect(game.flatten([])).to eq([])
    end
  end

  describe "#flatten" do
    it "returns empty array given invalid input" do
      game = ArrayHelper.new
      expect(game.flatten("invalid string")).to eq([])
      expect(game.flatten(1234)).to eq([])
      expect(game.flatten({"this" => "is", "a" => "dictionary"})).to eq([])
    end
  end
end