class ArrayHelper
  def flatten(nestedArray)
    if (!nestedArray.kind_of?(Array)) 
      return []
    end
    _flattenedArray = []
    nestedArray.each { |item|
      # puts item
      if (item.kind_of?(Array))
        _flattenedArray.concat flatten(item)
      else
        _flattenedArray << item
      end
      # puts _flattenedArray
    }
  return _flattenedArray
  end
end